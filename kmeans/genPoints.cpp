#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cmath>
#include <vector>
/***********************
	HEADER SECTION
************************/
/*
Point
	A NUM_DIM dimensional point for the use with the K means algorithm
	if NUM_DIM is altered the initialization array should be updated as well

	coords - the coordinates for the points
	label - the cluster that this point belongs to. -1 for uninitialized
*/
#define NUM_DIM 10
struct Point
{
	double coords[NUM_DIM] = {0,0,0,0,0,0,0,0,0,0};
	int label{-1};
};

//Create a type alias for readability
using DataVector = std::vector<Point>;

/*
Print a point
*/
void print_point(Point p)
{
	for (int i = 0; i < NUM_DIM; i++)
		printf("%lf,", p.coords[i]);
	printf("%d\n", p.label);
}

/**********************
	MAIN
***********************/
int main(int argc, char** argv)
{
	//Will use random numbers. Init srand
	srand(time(NULL));

	int num_points = argc >= 2 ? atoi(argv[1]) : 10;
	int num_dim = argc >= 3 ? atoi(argv[2]) : NUM_DIM;
	bool rando = argc >= 4 ? atoi(argv[3]) == 1 : false;
	int num_clusters = 9;
	Point p;

	//Initialize random data
	//printf("Before clustering:\n");
	if (rando)
	{
		for (int i = 0; i < num_points; i++)
		{
			for (int j = 0; j < num_dim; j++)
				p.coords[j] = (double) rand() / RAND_MAX * 100.0;
			p.label = rand() % num_clusters;
			print_point(p);
		}
	}
	else
	{	
		for (int i = 0; i < num_points; i++)
		{
			p.coords[0] = (i % 100);
			p.coords[1] = (double) (i / 100);
			p.label = rand() % num_clusters;
			print_point(p);
		}
	}

	return 0;
}