/*
MPI K Means Algorithm

An implementation of the kmeans algorithm using MPI for now with 10 dimensions and CL args
Author: Ben Burnett
date: 10-4-2018

compile with
	mpicxx -Wall -o kmeans kmeans.cpp
*/
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits> //has the numeric_limits. Used to initialize max double in kmeans
#include <mpi.h>
/***********************
	HEADER SECTION
************************/
/*
Point
	A NUM_DIM dimensional point for the use with the K means algorithm
	if NUM_DIM is altered the initialization array should be updated as well

	coords - the coordinates for the points
	label - the cluster that this point belongs to. -1 for uninitialized
*/
#define NUM_DIM 10
struct Point
{
	double coords[NUM_DIM] = {0,0,0,0,0,0,0,0,0,0};
	int label{-1};
};

MPI_Datatype create_point_mpitype()
{
	MPI_Datatype type, oldtypes[2];
	//int count = 2;
	int blockcounts[2];
	MPI_Aint offsets[2], extent, lb;
	
	offsets[0] = 0;
	oldtypes[0] = MPI_DOUBLE;
	blockcounts[0] = 10;
	MPI_Type_get_extent(MPI_DOUBLE, &lb, &extent);
	offsets[1] = 10 * extent;
	oldtypes[1] = MPI_INT;
	blockcounts[1] = 1;

	MPI_Type_create_struct(2, blockcounts, offsets, oldtypes, &type);
	return type;
}

//Create a type alias for readability
using DataVector = std::vector<Point>;

/*
Print a point
*/
void print_point(Point p)
{
	for (int i = 0; i < NUM_DIM; i++)
		printf("%lf,", p.coords[i]);
	printf("%d\n", p.label);
}

/*
Euclidian Distance Function
	Calculate the Euclidian distance sqrt((x1 - x2)^2 - (y1 - y2)^2)
	a - The first point
	b - The second point

	returns a double representing the distance
*/
double euclidian_distance(Point a, Point b)
{
	double res = 0.0;
	for (int i = 0; i < NUM_DIM; i++)
		res += (a.coords[i] - b.coords[i]) * (a.coords[i] - b.coords[i]);
	return sqrt(res);
}

/*
Read a file in
	Read in a file and store each line in a vector
	fname - the filename

	returns a vector with all all of the lines in the file
*/
std::vector<std::string> readfile(std::string fname)
{
	std::vector<std::string> lines;
	std::ifstream f(fname);

	if (f.is_open())
	{
		std::string line;
		while (std::getline(f, line))
			lines.push_back(line);
	}

	return lines;
}

/*
Get points from a file
	Uses the readfile function to get point data from a file

	fname - the filename

	returns a vector with all of the point data
*/

DataVector getpoints(std::string fname)
{
	std::vector<std::string> lines = readfile(fname);
	DataVector ret(lines.size());

	for (unsigned int i = 0; i < lines.size(); i++)
	{
		std::istringstream line(lines[i]);
		std::string val;
		for (int j = 0; j < NUM_DIM; j++)
		{
			std::getline(line, val, ',');
			ret[i].coords[j] = stod(val);
		}
		std::getline(line, val, ',');
		ret[i].label = stoi(val);
		//line >> ret[i].label;
	}

	return ret;
}

/**********************
	MAIN
***********************/
int main(int argc, char** argv)
{
	//Will use random numbers. Init srand
	srand(time(NULL));

	//begin: mpi initialization
	int rank, world_size;
	MPI_Status stat;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	MPI_Datatype pointtype = create_point_mpitype();
	MPI_Type_commit(&pointtype);
	//end: mpi initialization

	//begin: Point initialization
	int num_clusters = 8;
	DataVector centers(num_clusters);
	DataVector d = getpoints("points.csv");
	unsigned int num_points = d.size();
	unsigned int amt_per_rank = num_points / world_size;
	int* rank_amts = (int*)malloc(sizeof(int) * world_size);
	int* rank_displs = (int*)malloc(sizeof(int) * world_size);
	for (int i = 0; i < world_size; i++)
	{
		rank_amts[i] = rank == world_size ? amt_per_rank + (num_points % world_size) : amt_per_rank;
		rank_displs[i] = amt_per_rank * i;
	}
	unsigned int my_work = amt_per_rank * rank;
	unsigned int next_rank_work = rank < world_size ? amt_per_rank * (rank + 1) : num_points;
	
	if (rank == 0)
	{
		//have rank 0 initialize centers
		for (int c = 0; c < num_clusters; c++)
		{
			centers[c] = d[rand() % num_points];
		}
	}
	MPI_Bcast(&centers[0], num_clusters, pointtype, 0, MPI_COMM_WORLD);
	//end: point initialization

	//begin: kmeans calculation
	double time = MPI_Wtime();
	int num_it = 40;
	for (int it = 0; it < num_it; it++)
	{
		//determine closest cluster and assign to that cluster
		for (unsigned int p = my_work; p < next_rank_work; p++) //p = point
		{
			double shortest_dist = std::numeric_limits<double>::max(); //initialize the shortest distance to max double value
			int closest_k = d[p].label;
			for (int c = 0; c < num_clusters; c++) //c = cluster
			{
				double dist = euclidian_distance(d[p], centers[c]);
				if (dist < shortest_dist)
				{
					shortest_dist = dist;
					closest_k = c;
				}
			}
			d[p].label = closest_k; //update label in local my_points for gathering
		}

		//recalculate the centers of the clusters based off of new assignments
		//The center is just the average x, y position
		DataVector new_centers(num_clusters);
		std::vector<int> num_pts_in_cluster(num_clusters, 0); //number of points in each cluster initialized to 0
		for (unsigned int p = my_work; p < next_rank_work; p++)
		{
			for (int i = 0; i < NUM_DIM; i++)
				new_centers[d[p].label].coords[i] += d[p].coords[i];
			num_pts_in_cluster[d[p].label] += 1;
		}
		for (int c = 0; c < num_clusters; c++)
		{
			int count;
			MPI_Reduce(&num_pts_in_cluster[c], &count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
			count = std::max<int>(1, count); //make sure division by zero is not possible in case a cluster has no points
			for (int i = 0; i < NUM_DIM; i++)
			{
				//each rank is going to have a sum of each coord, so collect and then calc ave
				//afterwards send out to all ranks
				//QUESTION THOUGH
				//Is it better to do reductions over each coord or send to rank 0 the 
				//num_pts_in_cluster and new centers from each rank?
				//might be interesting to look into this...
				double coord = 0.0;
				MPI_Reduce(&new_centers[c].coords[i], &coord, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
				if (rank == 0)
					centers[c].coords[i] = coord / count;
			}
		}
		MPI_Bcast(&centers[0], num_clusters, pointtype, 0, MPI_COMM_WORLD); //rebroadcast after updating all centers
	}
	if (rank == 0)
		fprintf(stderr, "Kmeans algorithm time: %lf\n", MPI_Wtime() - time);
	//end: kmeans calculation

	//collect all of the updated label information in rank 0
	for (int i = 1; i < world_size; i++)
	{
		if (rank == i)
		{
			MPI_Send(&d[my_work], amt_per_rank, pointtype, 0, 0, MPI_COMM_WORLD);
		}
		else if (rank == 0)
		{
			int recv_rank_work;
			if (i == 0)
				recv_rank_work = 0;
			else 
				recv_rank_work = amt_per_rank * i;
			MPI_Recv(&d[recv_rank_work], amt_per_rank, pointtype, i, 0, MPI_COMM_WORLD, &stat);
		}
	}

	//begin: print results
	if (rank == 0)
	{
		printf("Points:\n");
		for (unsigned int i = 0; i < num_points; i++)
			print_point(d[i]);
		printf("Centers:\n");
		for (int i = 0; i < num_clusters; i++)
		{
			centers[i].label = i;
			print_point(centers[i]);
		}
	}
	//end: print results

	//begin: mpi exit
	MPI_Type_free(&pointtype);
	MPI_Finalize();
	//end: mpi exit
	free(rank_amts);
	free(rank_displs);
	return 0;
}