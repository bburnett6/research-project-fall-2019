"""
Plot's points generated from the kmeans.cpp program

run kmeans like:
 $> ./kmeans > out.txt
then run python script like
 $> python plot_points.py

"""
import matplotlib.pyplot as plt 
import sys

if len(sys.argv) < 2:
	print("usage: python plot_points.py fname")
	sys.exit()

points = []
centers = []
with open(sys.argv[1], 'r') as f:
	f.readline()
	data = []
	for line in f.readlines():
		if 'Centers' in line:
			points = data
			data = []
		else:
			sp = line.split(',')
			data.append({'x' : float(sp[0]), 'y' : float(sp[1]), 'label' : int(sp[-1])})
	centers = data

num_centers = len(centers)
c = []
for i in range(num_centers):
	c.append(list(filter(lambda x : x['label'] == i, points)))

colors = ['']

for i in range(num_centers):
	plt.plot(list(map(lambda x: x['x'], c[i])), list(map(lambda x: x['y'], c[i])), '.')

plt.plot(list(map(lambda x: x['x'], centers)), list(map(lambda x: x['y'], centers)), 'k*')
plt.show()