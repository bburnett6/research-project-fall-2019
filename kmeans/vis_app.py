"""
A simple visualization of the kmeans algorithm using pygame as the rendering engine.
Author: Ben Burnett
Date: 10-20-2019

requires: pygame (available with pip: 'pip install pygame')

run with:
	python vis_app.py width height dx dy k
	where width, height are the dimensions of the screen, dx, dy are the size of the points,
	and k is the number of clusters.
"""

import pygame
from math import sqrt
from pygame.locals import *
import random
import sys 

class Point:
	"""
	A simple 2D point class with implementations of the Minkowski and Manhattan Distances
	"""
	def __init__(self, x, y, l):
		self.x = x 
		self.y = y 
		self.label = l 

	"""
	Minkowski distance is a class of distances. Use p = 2 for euclidian
		b - the second point
		p - distance class parameter
	"""
	def minkowski_dist(self, b, p=2):
		dx = self.x - b.x
		dy = self.y - b.y
		return pow(pow(dx, p) + pow(dy, p), 1.0/float(p))

	def manhattan_dist(self, b):
		dx = self.x - b.x
		dy = self.y - b.y
		return abs(dx) + abs(dy)

"""
Generate a bunch of points with random labels
	x - the upper limit of the x-axis
	y - the upper limit of the y-axis
	dx - the increase step of x
	dy - the increase step of y
	k - the number of labels that can be generated (0 to k)

	defaults are set to make a 500x500 grid
"""
def gen_points(x=500, y=500, dx=5, dy=5, k=9):
	points = []
	for i in range(0, x, dx):
		for j in range(0, y, dy):
			points.append(Point(i, j, random.randint(0, k)))
	return points 

"""
Generate a bunch of points in gaussian clusters with random labels
	x - The upper limit of the x-axis
	y - the upper limit of the y-axis
	n - the number of points
	k - the number of clusters
"""
def gen_gauss_pts(x=500, y=500, n=100, k=9):
	points = []
	f = lambda mu, sigma : random.gauss(mu, sigma)
	for i in range(k):
		centerx = random.uniform(50, x - 50)
		centery = random.uniform(50, y - 50)
		sig = random.uniform(0, 50)
		for j in range(n//k):
			points.append(Point(f(centerx, sig), f(centery, sig), random.randint(0, k)))
	return points 

"""
Get a color from an int! No general correlation here.

Colors from https://www.rapidtables.com/web/color/RGB_Color.html
"""
def get_color(k):
	if k == 0: 
		return (255,0,0) #red
	elif k == 1: 
		return (0,255,0) #green
	elif k == 2:
		return (0,0,255) #blue
	elif k == 3:
		return (0,0,128) #dark blue
	elif k == 4:
		return (255, 200, 200) #pink
	elif k == 5:
		return (0, 255, 255) #cyan
	elif k == 6:
		return (255, 0, 255) #magenta
	elif k == 7:
		return (255, 99, 71) #"tomato"
	elif k == 8:
		return (255, 140, 0) #orange
	elif k == 9:
		return (218, 165, 32) #golden rod
	elif k == 10:
		return (0, 128, 0) #dark green
	elif k == 11:
		return (32, 178, 170) #light sea green
	elif k == 12:
		return (176, 224, 230) #powder blue
	elif k == 13:
		return (100, 149, 237) #corn flower blue
	elif k == 14:
		return (138, 43, 226) #blue violet
	elif k == 15:
		return (210, 105, 30) #chocolate
	else:
		return (0, 0, 0) #black

class App:
	"""
	The main app class. This app is a simple implementation of the K-Means Algorithm
	with the intent to visualize it. This class uses the provided Point class with 
	its provided distance functions to show off what they do. 
	This class is split up into several base functions with the following purposes.
		on_init - initialize pygame, set the display mode, and initialize the points
			and clusters for visualization.
		on_event - handle pygame events. Currently only watches for quit via gui 
			and q key
		on_loop - The major loop of the simulation. This funcion houses the
			implementation of the kmeans algorithm.
		on_render - draws the points as rectangles and the cluster centers as dots.
			Also handles update of the display surface
		on_cleanup - handles any cleanup of the program including quiting pygame
		run - The main logical loop of the simulation. Is to be called by
			a main driver. This is the function that calls all other functions in
			this class.

	init arguments:
		w - the width of the screen
		h - the height of the screen
		dx - the x size of each point (only for visualizing has no effect on the simulation)
		dy - the y size of each point (only for visualizing)
		k - the number of clusters

	"""
	def __init__(self, w=500, h=500, dx=5, dy=5, k=9):
		self._running = True
		self._display_surf = None
		self.size = self.width, self.height = w, h
		self.numk = k
		self.dx = dx
		self.dy = dy 
		self.points = []
		self.clusters = []
		self.newclusters = []

	def on_init(self):
		pygame.init()
		pygame.display.set_caption('K-Means with %s Clusters' % self.numk)
		self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
		self._running = True
		#self.points = gen_points(self.width, self.height, self.dx, self.dy, self.numk)
		self.points = gen_gauss_pts(self.width, self.height, 10000, self.numk)
		for i in range(self.numk):
			#p = self.points[random.randint(0, len(self.points) - 1)]
			#self.clusters.append(Point(p.x, p.y, i))
			self.clusters.append(Point(random.randint(0, self.width), random.randint(0, self.height), i))
			self.newclusters.append(Point(0, 0, i))

	def on_event(self, event):
		if event.type == pygame.QUIT:
			self._running = False
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_q:
				self._running = False

	def on_loop(self):
		#Update the points labels
		for p in self.points:
			shortest_dist = self.height * self.width * self.numk
			closest_k = p.label 
			for k in self.clusters:
				dist = k.minkowski_dist(p)
				#dist = k.manhattan_dist(p)
				#dist = k.minkowski_dist(p, 6)
				if dist < shortest_dist:
					shortest_dist = dist 
					closest_k = k.label
			p.label = closest_k
		#Update the clusters center
		num_points = [0 for _ in range(self.numk)]
		for p in self.points:
			self.newclusters[p.label].x = self.newclusters[p.label].x + p.x 
			self.newclusters[p.label].y = self.newclusters[p.label].y + p.y 
			num_points[p.label] = num_points[p.label] + 1
		for k in self.clusters:
			count = max(1, num_points[k.label])
			k.x = self.newclusters[k.label].x / count
			k.y = self.newclusters[k.label].y / count
		#reset newclusters
		for k in self.newclusters:
			k.x = 0
			k.y = 0

	def on_render(self):
		self._display_surf.fill((255,255,255))
		for p in self.points:
			#pygame.draw.rect(self._display_surf, get_color(p.label), (p.x, p.y, self.dx, self.dy), 0)
			pygame.draw.circle(self._display_surf, get_color(p.label), (int(p.x), int(p.y)), 2, 0)
		for k in self.clusters:
			pygame.draw.circle(self._display_surf, get_color(-1), (int(k.x), int(k.y)), 4, 0)
		pygame.display.update()

	def on_cleanup(self):
		print("Bye bye")
		pygame.quit()

	def run(self):
		if self.on_init() == False:
			self._running = False

		while(self._running):
			for event in pygame.event.get():
				self.on_event(event)
			self.on_loop()
			self.on_render()
		self.on_cleanup()

if __name__ == '__main__':
	# simple command line argument handling.
	w = int(sys.argv[1]) if len(sys.argv) > 1 else 500
	h = int(sys.argv[2]) if len(sys.argv) > 2 else 500
	dx = int(sys.argv[3]) if len(sys.argv) > 3 else 5
	dy = int(sys.argv[4]) if len(sys.argv) > 4 else 5
	k = int(sys.argv[5]) if len(sys.argv) > 5 else 9
	# init and run the app
	app = App(w, h, dx, dy, k)
	app.run()
