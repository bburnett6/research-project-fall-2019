
import numpy as np 
import matplotlib.pyplot as plt 
from celluloid import Camera
import random
import copy

class Point:
	"""
	A simple 2D point class with implementations of the Minkowski and Manhattan Distances
	"""
	def __init__(self, x, y, l):
		self.x = x 
		self.y = y 
		self.label = l 

	def __str__(self):
		return f'{self.x},{self.y},{self.label}'

	"""
	Minkowski distance is a class of distances. Use p = 2 for euclidian
		b - the second point
		p - distance class parameter
	"""
	def minkowski_dist(self, b, p=2):
		dx = self.x - b.x
		dy = self.y - b.y
		return pow(pow(dx, p) + pow(dy, p), 1.0/float(p))

	def manhattan_dist(self, b):
		dx = self.x - b.x
		dy = self.y - b.y
		return abs(dx) + abs(dy)

"""
Generate a bunch of points in gaussian clusters with random labels
	x - The upper limit of the x-axis
	y - the upper limit of the y-axis
	n - the number of points
	k - the number of clusters
"""
def gen_gauss_pts(x=500, y=500, n=100, k=9):
	points = []
	f = lambda mu, sigma : random.gauss(mu, sigma)
	for i in range(k):
		centerx = random.uniform(50, x - 50)
		centery = random.uniform(50, y - 50)
		sig = random.uniform(0, 50)
		for j in range(n//k):
			points.append(Point(f(centerx, sig), f(centery, sig), random.randint(0, k)))
	return points 

def kmeans(pts, x=500, y=500, n=100, numk=9, iterations=20):
	points = copy.deepcopy(pts)
	fig = plt.figure()
	camera = Camera(fig)
	clusters = []
	newclusters = []
	for i in range(numk):
			clusters.append(Point(random.randint(0, x), random.randint(0, y), i))
			newclusters.append(Point(0, 0, i))

	#Update the points labels
	for i in range(iterations):
		for p in points:
			shortest_dist = x * y * numk 
			closest_k = p.label 
			for k in clusters:
				dist = k.minkowski_dist(p)
				#dist = k.manhattan_dist(p)
				#dist = k.minkowski_dist(p, 6)
				if dist < shortest_dist:
					shortest_dist = dist 
					closest_k = k.label
			p.label = closest_k
		#Update the clusters center
		num_points = [0 for _ in range(numk)]
		for p in points:
			newclusters[p.label].x = newclusters[p.label].x + p.x 
			newclusters[p.label].y = newclusters[p.label].y + p.y 
			num_points[p.label] = num_points[p.label] + 1
		for k in clusters:
			count = max(1, num_points[k.label])
			k.x = newclusters[k.label].x / count
			k.y = newclusters[k.label].y / count
		#reset newclusters
		for k in newclusters:
			k.x = 0
			k.y = 0
		for cluster in clusters:
			group = filter(lambda p : p.label == cluster.label, points)
			grp_x, grp_y, _ = point_group_to_array(group)
			plt.scatter(grp_x, grp_y, s=1)
		k_x, k_y, _ = point_group_to_array(clusters)
		plt.scatter(k_x, k_y, color='black', marker='*')
		camera.snap()

	animation = camera.animate()
	animation.save('kmeans.mp4')
	return (points, clusters)

"""
Takes a list of points and turns it into 3 arrays of the x and y coordinates
and the labels. This is helpful for plotting and training
"""
def point_group_to_array(pts):
	x = list()
	y = list()
	l = list() 
	for p in pts:
		x.append(p.x)
		y.append(p.y)
		l.append(p.label)
	return (x, y, l)

"""
Plot the output of the kmeans function. The cluster centers are black *'s 
and the points themselves are color coordinated to which center they belong to
"""
def plot_kmeans(pts, clusters):
	for cluster in clusters:
		group = filter(lambda p : p.label == cluster.label, pts)
		grp_x, grp_y, _ = point_group_to_array(group)
		plt.scatter(grp_x, grp_y, s=1)
	k_x, k_y, _ = point_group_to_array(clusters)
	plt.scatter(k_x, k_y, color='black', marker='*')
	plt.show()


def main():
	x = 1000
	y = 1000
	num_points = 10000
	numk = 10
	iterations = 25
	pre_pts = gen_gauss_pts(x, y, num_points, numk)
	post_pts, cs = kmeans(pre_pts, x, y, num_points, numk, iterations)

if __name__ == '__main__':
	main()