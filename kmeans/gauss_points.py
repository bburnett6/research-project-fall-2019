import numpy as np 
import sys
import random

fname = sys.argv[1] if len(sys.argv) > 1 else "pts.csv"
n = int(sys.argv[2]) if len(sys.argv) > 2 else 500
k = int(sys.argv[3]) if len(sys.argv) > 3 else 2

n_per_k = n // k
ndim = 10

with open(fname, 'w') as f:
	for ki in range(k):
		centers = []
		for i in range(ndim):
			centers.append(random.uniform(50, 450))
		#center = random.uniform(0, 450)
		sigma = random.uniform(0, 20)
		ks = np.random.normal(centers, sigma, (n_per_k, ndim))
		for col in ks:
			s = ""
			for i, val in enumerate(col):
				s = s + str(val if i < 2 else 0.0) + ','
			f.write("%s%s\n" % (s,random.randint(0,k)))