/*
K Means Algorithm

A simple implementation of the kmeans algorithm for now with 10 dimensions and CL args
Author: Ben Burnett
date: 10-4-2018

compile with (needs mpi for timer)
	mpicxx -Wall -o kmeans kmeans.cpp 
*/
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits> //has the numeric_limits. Used to initialize max double in kmeans
#include <mpi.h>
/***********************
	HEADER SECTION
************************/
/*
Point
	A NUM_DIM dimensional point for the use with the K means algorithm
	if NUM_DIM is altered the initialization array should be updated as well

	coords - the coordinates for the points
	label - the cluster that this point belongs to. -1 for uninitialized
*/
#define NUM_DIM 10
struct Point
{
	double coords[NUM_DIM] = {0,0,0,0,0,0,0,0,0,0};
	int label{-1};
};

//Create a type alias for readability
using DataVector = std::vector<Point>;

/*
Print a point
*/
void print_point(Point p)
{
	for (int i = 0; i < NUM_DIM; i++)
		printf("%lf,", p.coords[i]);
	printf("%d\n", p.label);
}

/*
Euclidian Distance Function
	Calculate the Euclidian distance sqrt((x1 - x2)^2 - (y1 - y2)^2) but for 10D
	a - The first point
	b - The second point

	returns a double representing the distance
*/
double euclidian_distance(Point a, Point b)
{
	double res = 0.0;
	for (int i = 0; i < NUM_DIM; i++)
		res += (a.coords[i] - b.coords[i]) * (a.coords[i] - b.coords[i]);
	return sqrt(res);
}

/*
Read a file in
	Read in a file and store each line in a vector
	fname - the filename

	returns a vector with all all of the lines in the file
*/
std::vector<std::string> readfile(std::string fname)
{
	std::vector<std::string> lines;
	std::ifstream f(fname);

	if (f.is_open())
	{
		std::string line;
		while (std::getline(f, line))
			lines.push_back(line);
	}

	return lines;
}

/*
Get points from a file
	Uses the readfile function to get point data from a file

	fname - the filename

	returns a vector with all of the point data
*/

DataVector getpoints(std::string fname)
{
	std::vector<std::string> lines = readfile(fname);
	DataVector ret(lines.size());

	for (unsigned int i = 0; i < lines.size(); i++)
	{
		std::istringstream line(lines[i]);
		std::string val;
		for (int j = 0; j < NUM_DIM; j++)
		{
			std::getline(line, val, ',');
			ret[i].coords[j] = stod(val);
		}
		std::getline(line, val, ',');
		ret[i].label = stoi(val);
	}

	return ret;
}

/*
K Means Algorithm

	data - A vector containing the data to cluster. Will be modified.
	k - the number of clusters
	num_it - the number of iterations to run

	returns a vector containing the centers
Notes:
	Pass the data vector in as a reference so we can modify its label
*/
DataVector kmeans(DataVector &data, int k, int num_it)
{

	//Initialize the centers to a random point
	DataVector centers(k);
	for (int c = 0; c < k; c++)
		centers[c] = data[rand() % data.size()];

	for (int it = 0; it < num_it; it++)
	{
		//determine closest cluster and assign to that cluster
		for (unsigned int p = 0; p < data.size(); p++) //p = point
		{
			double shortest_dist = std::numeric_limits<double>::max(); //initialize the shortest distance to max double value
			int closest_k = data[p].label;
			for (int c = 0; c < k; c++) //c = cluster
			{
				double dist = euclidian_distance(data[p], centers[c]);
				if (dist < shortest_dist)
				{
					shortest_dist = dist;
					closest_k = c;
				}
			}
			data[p].label = closest_k;
		}

		//recalculate the centers of the clusters based off of new assignments
		//The center is just the average x, y position
		DataVector new_centers(k);
		std::vector<int> num_points(k, 0); //number of points in each cluster initialized to 0
		for (unsigned int p = 0; p < data.size(); p++)
		{
			for (int i = 0; i < NUM_DIM; i++)
				new_centers[data[p].label].coords[i] += data[p].coords[i];
			num_points[data[p].label] += 1;
		}
		for (int c = 0; c < k; c++)
		{
			int count = std::max<int>(1, num_points[c]); //make sure division by zero is not possible in case a cluster has no points
			for (int i = 0; i < NUM_DIM; i++)
				centers[c].coords[i] = new_centers[c].coords[i] / count;
		}
	}

	return centers;
}

/**********************
	MAIN
***********************/
int main()
{
	//Will use random numbers. Init srand
	srand(time(NULL));

	int num_clusters = 9;
	DataVector d = getpoints("points.csv");
	int num_points = d.size();

	double time = MPI_Wtime();
	DataVector cent = kmeans(d, num_clusters, 100);
	fprintf(stderr, "Kmeans algorithm time: %lf\n", MPI_Wtime() - time);

	printf("Points:\n");
	for (int i = 0; i < num_points; i++)
		print_point(d[i]);
	printf("Centers:\n");
	for (int i = 0; i < num_clusters; i++)
	{
		cent[i].label = i;
		print_point(cent[i]);
	}

	return 0;
}