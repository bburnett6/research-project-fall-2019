import torch
import torch.nn as nn
import torch.nn.functional as F 
from torch.autograd import Variable

import matplotlib.pyplot as plt 
import numpy as np 
from celluloid import Camera

class Net(nn.Module):
	def __init__(self):
		super(Net, self).__init__()
		self.layer = torch.nn.Linear(1, 1)

	def forward(self, x):
		x = self.layer(x)
		return x 

##############
# VARIABLES 
##############
n = Net()
x = np.random.rand(100)
y = np.sin(x) * np.power(x, 3) + 3 * x + np.random.rand(100) * 0.8

tx = torch.from_numpy(x.reshape(-1, 1)).float()
ty = torch.from_numpy(y.reshape(-1, 1)).float()

optimizer = torch.optim.SGD(n.parameters(), lr=0.2)
loss_fxn = torch.nn.MSELoss()
n_epochs = 250

############
# TRAINING
############

ins = Variable(tx)
outs = Variable(ty)
#print(ins)
#print(outs)
fig = plt.figure()
camera = Camera(fig)
for epoch in range(n_epochs):
	prediction = n(ins)
	loss = loss_fxn(prediction, outs)
	optimizer.zero_grad()
	loss.backward() 
	optimizer.step()

	
	if epoch % 5 == 0:
		#plt.cla()
		plt.scatter(x, y, color='blue')
		plt.plot(x, prediction.data.numpy(), 'r-', lw=2)
		plt.text(0.5, 0.5, f"Loss={loss.data.numpy()}", fontdict={'size':12, 'color': 'red'})
		#plt.pause(0.1)
		#plt.show()
		camera.snap()
	
animation = camera.animate()
animation.save('linear.mp4')
############
# PLOTTING
############

#plt.scatter(x, y)
#plt.show()