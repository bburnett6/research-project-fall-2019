#Example of a function classifier with 3 classes using pytorch
#original example from: https://cs.stanford.edu/people/karpathy/cs231nfiles/minimal_net.html
#I was able to reproduce the example using pytorch
#This is neat because you can see what the effects of the hyper parameters do

import numpy as np 
import matplotlib.pyplot as plt 
from celluloid import Camera
import torch
import torch.nn as nn
import torch.nn.functional as F 
from torch.autograd import Variable

class Net(nn.Module):
	def __init__(self):
		super(Net, self).__init__()
		self.layer1 = torch.nn.Linear(2, 100)
		self.layer2 = torch.nn.Linear(100, 3)

	def forward(self, x):
		x = F.relu(self.layer1(x))
		x = F.log_softmax(self.layer2(x), dim=1)
		return x 

###################
# Prepare the data
###################
N = 100
D = 2
K = 3
X = np.zeros((N * K, D), dtype='float32')
Y = np.zeros(N * K, dtype='int')
for j in range(K):
	ix = range(N * j, N * (j + 1))
	r = np.linspace(0.0, 1, N)
	t = np.linspace(j * 4, (j + 1) * 4, N) + np.random.randn(N) * 0.2
	X[ix] = np.c_[r * np.sin(t), r * np.cos(t)]
	Y[ix] = j

##########################
# Prepare the plot stuff
##########################
fig = plt.figure()
camera = Camera(fig)
h = 0.02
x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h, dtype='float32'), np.arange(y_min, y_max, h, dtype='float32'))
test_data = torch.tensor(np.c_[xx.ravel(), yy.ravel()])

#######################
# Train the Neural Net
#######################
datax = torch.tensor(X)
datay = torch.tensor(Y)
learning_rate = 0.2
m = 0.9
net = Net()
optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate, momentum=m)
loss_fxn = torch.nn.CrossEntropyLoss(reduction='mean')
n_epochs = 250
ins = Variable(datax)
outs = Variable(datay)
for epoch in range(n_epochs):
	prediction = net(ins)
	loss = loss_fxn(prediction, outs)
	optimizer.zero_grad()
	loss.backward() 
	optimizer.step()

	if epoch % 5 == 0:
		#print( f"Loss={loss.data.numpy()}" )
		test_out = net(test_data)
		Z = test_out.data.numpy()
		Z = np.argmax(Z, axis=1)
		Z = Z.reshape(xx.shape)
		plt.contourf(xx, yy, Z, cmap=plt.cm.winter, alpha=0.5)
		plt.scatter(X[:, 0], X[:, 1], c=Y, s=40, cmap=plt.cm.winter)
		plt.text(-0.5, -1.5, f"Loss={loss.data.numpy()}", fontdict={'size':12, 'color': 'red'})
		plt.xlim(xx.min(), xx.max())
		plt.ylim(yy.min(), yy.max())
		#plt.show()
		camera.snap()

animation = camera.animate()
animation.save('spiral.mp4')
"""
# I put all of this inside the training loop to show the learning take place
######################################################
# Use the Neural Net on Sample Data.
# In this case the sample data is a meshgrid of values 
# used to visualize the trained networks response to 
# different values
######################################################
h = 0.02
x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h, dtype='float32'), np.arange(y_min, y_max, h, dtype='float32'))
test_data = torch.tensor(np.c_[xx.ravel(), yy.ravel()])
test_out = net(test_data)
Z = test_out.data.numpy()
Z = np.argmax(Z, axis=1)
Z = Z.reshape(xx.shape)

###################
# Plot the results
###################
fig = plt.figure()
plt.contourf(xx, yy, Z, cmap=plt.cm.winter, alpha=0.5)
plt.scatter(X[:, 0], X[:, 1], c=Y, s=40, cmap=plt.cm.winter)
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.show()
"""