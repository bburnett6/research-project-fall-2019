"""
This script is attempting to use an auto encoder neural network to see if it can
correcly label points similar to a kmeans cluster. This is done by generating many
samples of a kmeans algorithm (using a static seed for reproducibility) and using 
the pre clustered points as inputs and the clustered points as outputs during the
training process.

Author: Ben Burnett
"""
import torch
import torch.nn as nn
import torch.nn.functional as F 
from torch.autograd import Variable
torch.manual_seed(123)

import numpy as np 
import random
import sys 
import matplotlib.pyplot as plt
from celluloid import Camera 
import copy

#############################################
# Begin Kmeans Helper Classes and Functions
#############################################

class Point:
	"""
	A simple 2D point class with implementations of the Minkowski and Manhattan Distances
	"""
	def __init__(self, x, y, l):
		self.x = x 
		self.y = y 
		self.label = l 

	def __str__(self):
		return f'{self.x},{self.y},{self.label}'

	"""
	Minkowski distance is a class of distances. Use p = 2 for euclidian
		b - the second point
		p - distance class parameter
	"""
	def minkowski_dist(self, b, p=2):
		dx = self.x - b.x
		dy = self.y - b.y
		return pow(pow(dx, p) + pow(dy, p), 1.0/float(p))

	def manhattan_dist(self, b):
		dx = self.x - b.x
		dy = self.y - b.y
		return abs(dx) + abs(dy)

"""
Generate a bunch of points in gaussian clusters with random labels
	x - The upper limit of the x-axis
	y - the upper limit of the y-axis
	n - the number of points
	k - the number of clusters
"""
def gen_gauss_pts(x=500, y=500, n=100, k=9):
	points = []
	f = lambda mu, sigma : random.gauss(mu, sigma)
	for i in range(k):
		centerx = random.uniform(50, x - 50)
		centery = random.uniform(50, y - 50)
		sig = random.uniform(0, 50)
		for j in range(n//k):
			points.append(Point(f(centerx, sig), f(centery, sig), random.randint(0, k)))
	return points 

def kmeans(pts, x=500, y=500, n=100, numk=9, iterations=20):
	points = copy.deepcopy(pts)
	clusters = []
	newclusters = []
	for i in range(numk):
			clusters.append(Point(random.randint(0, x), random.randint(0, y), i))
			newclusters.append(Point(0, 0, i))

	#Update the points labels
	for i in range(iterations):
		for p in points:
			shortest_dist = x * y * numk 
			closest_k = p.label 
			for k in clusters:
				dist = k.minkowski_dist(p)
				#dist = k.manhattan_dist(p)
				#dist = k.minkowski_dist(p, 6)
				if dist < shortest_dist:
					shortest_dist = dist 
					closest_k = k.label
			p.label = closest_k
		#Update the clusters center
		num_points = [0 for _ in range(numk)]
		for p in points:
			newclusters[p.label].x = newclusters[p.label].x + p.x 
			newclusters[p.label].y = newclusters[p.label].y + p.y 
			num_points[p.label] = num_points[p.label] + 1
		for k in clusters:
			count = max(1, num_points[k.label])
			k.x = newclusters[k.label].x / count
			k.y = newclusters[k.label].y / count
		#reset newclusters
		for k in newclusters:
			k.x = 0
			k.y = 0

	return (points, clusters)

"""
Takes a list of points and turns it into 3 arrays of the x and y coordinates
and the labels. This is helpful for plotting and training
"""
def point_group_to_array(pts):
	x = list()
	y = list()
	l = list() 
	for p in pts:
		x.append(p.x)
		y.append(p.y)
		l.append(p.label)
	return (x, y, l)

"""
Plot the output of the kmeans function. The cluster centers are black *'s 
and the points themselves are color coordinated to which center they belong to
"""
def plot_kmeans(pts, clusters):
	for cluster in clusters:
		group = filter(lambda p : p.label == cluster.label, pts)
		grp_x, grp_y, _ = point_group_to_array(group)
		plt.scatter(grp_x, grp_y, s=1)
	k_x, k_y, _ = point_group_to_array(clusters)
	plt.scatter(k_x, k_y, color='black', marker='*')
	plt.show()

###############################################
# End Kmeans Helper Functions and Classes
###############################################

###############################################
# Start neural net functions and classes
###############################################

"""
GenTrainingData
	Used to generate training data for the KmeansNet. This can be optimized
	by using numpy.
"""
def genTrainingData(x, y, num_points, numk, iterations, num_training_samples):
	xs = list() #the inputs to the NN (the x, y, and random labels)
	ys = list() #the outputs of the NN (the x, y, and clustered labels)
	normalize = lambda x, m : map(lambda a : a / m, x)
	for i in range(num_training_samples):
		pre_pts = gen_gauss_pts(x, y, num_points, numk)
		post_pts, cs = kmeans(pre_pts, x, y, num_points, numk, iterations) #don't need the clutster centers really
		px, py, pl = point_group_to_array(pre_pts)
		ax, ay, al = point_group_to_array(post_pts)
		# The input layer is the points with randomly initialized labels
		# this random initialization will hopefully be recognized by the 
		# network as noise which it will then fix
		xs.append(list(normalize(px, x)) + list(normalize(py, y)) + pl)
		# the output layer is the post algorithm points. So the basic
		# idea is that the network will hopefully learn the algorithm
		ys.append(list(normalize(ax, x)) + list(normalize(ay, y)) + al)
	return (xs, ys)

"""
# Attempt 1
class KmeansNet(torch.nn.Module):
	def __init__(self, in_dim, hid_dim, out_dim):
		super(KmeansNet, self).__init__()
		self.inputlayer = nn.Linear(in_dim, hid_dim)
		self.hiddenlayer = nn.Linear(hid_dim, hid_dim)
		self.outlayer = nn.Linear(hid_dim, out_dim)

	def forward(self, x):
		x = F.relu(self.inputlayer(x))
		x = F.relu(self.hiddenlayer(x))
		x = self.outlayer(x)
		return x

"""

class KmeansNet(torch.nn.Module):
	def __init__(self, in_dim, hid_dim, code_dim):
		super(KmeansNet, self).__init__()
		self.encoder = torch.nn.Sequential(
			torch.nn.Linear(in_dim, hid_dim),
			torch.nn.ReLU(True),
			torch.nn.Linear(hid_dim, code_dim))
		self.decoder = torch.nn.Sequential(
			torch.nn.Linear(code_dim, hid_dim),
			torch.nn.ReLU(True),
			torch.nn.Linear(hid_dim, in_dim),
			torch.nn.Sigmoid())

	def forward(self, x):
		x_hat = self.encoder(x)
		x_hat = self.decoder(x_hat)
		return x_hat

###############################################
# End neural net functions and classes
###############################################

#########
# MAIN
#########

def main():
	# Constants
	seed = 6
	random.seed(seed) #Seed the random number generator for reproducability
	width = 500 #the coordinate space for x, (0, 500)
	height = 500 #the coordinate space for y, (0, 500)
	num_points = 1000
	numk = 10 
	num_iters = 25
	num_samples = 60

	#Prepare plotting stuff
	fig = plt.figure()
	camera = Camera(fig)
	ax1 = plt.subplot(131)
	ax1.set_xlim([-0.1, 1.1])
	ax1.set_ylim([-0.1, 1.1])
	ax2 = plt.subplot(132)
	ax2.set_xlim([-0.1, 1.1])
	ax2.set_ylim([-0.1, 1.1])
	ax3 = plt.subplot(133)
	ax3.set_xlim([-0.1, 1.1])
	ax3.set_ylim([-0.1, 1.1])
	testx, testy = genTrainingData(width, height, num_points, numk, num_iters, 1)
	inx = torch.tensor(testx[0])

	# Train The Neural net
	datax, datay = genTrainingData(width, height, num_points, numk, num_iters, num_samples)
	datax = torch.tensor(datax)
	datay = torch.tensor(datay)
	net = KmeansNet(num_points * 3, 100, num_points * 3)
	learning_rate = 0.2
	m = 0.9
	optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate, momentum=m)
	#optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
	loss_fxn = torch.nn.MSELoss()
	#loss_fxn = torch.nn.BCELoss()
	n_epochs = 2000000

	ins = Variable(datax)
	outs = Variable(datay)
	for epoch in range(n_epochs):
		prediction = net(ins)
		loss = loss_fxn(prediction, outs)
		optimizer.zero_grad()
		loss.backward() 
		optimizer.step()
		
		if epoch % 1000 == 0:
			predy = net(inx)
			before = testx[0]
			ax1.scatter(before[:num_points], before[num_points:num_points*2], c=before[num_points*2:], cmap=plt.cm.Set2)
			pred = predy.data.numpy()
			pred_labels = list(map(lambda x : int(x), pred[num_points*2:]))
			ax2.scatter(pred[:num_points], pred[num_points:num_points*2], c=pred_labels, cmap=plt.cm.Set2)
			ax2.text(0.0, 0.0, f"Loss={loss.data.numpy()}", fontdict={'size':12, 'color': 'red'})
			actual = testy[0]
			ax3.scatter(actual[:num_points], actual[num_points:num_points*2], c=actual[num_points*2:], cmap=plt.cm.Set2)
			#plt.show()
			camera.snap()

	animation = camera.animate()
	animation.save('autoen2_kmeans.mp4')

if __name__ == '__main__':
	main()