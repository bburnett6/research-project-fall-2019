"""
This script is able to learn the output of a kmeans clustering output. The paragraph below 
describes the original intent. Using neural nets, we can classify a point as belonging
to one of k clusters. This more closely emulates the k nearest neighbors algorithm.

The original intention of this script was to be able to generate a lot of kmeans runs for the
purpose of training a neural network to output the same results. This will hopefully
save from having to generate many files of data. Reproducability comes from using a
seed in the random number generation (python random package) and the pytorch neural 
network (python torch package).

Author: Ben Burnett
"""
import torch
import torch.nn as nn
import torch.nn.functional as F 
from torch.autograd import Variable

import numpy as np 
import random
import sys 
import matplotlib.pyplot as plt
from celluloid import Camera 

#############################################
# Begin Kmeans Helper Classes and Functions
#############################################

class Point:
	"""
	A simple 2D point class with implementations of the Minkowski and Manhattan Distances
	"""
	def __init__(self, x, y, l):
		self.x = x 
		self.y = y 
		self.label = l 

	def __str__(self):
		return f'{self.x},{self.y},{self.label}'

	"""
	Minkowski distance is a class of distances. Use p = 2 for euclidian
		b - the second point
		p - distance class parameter
	"""
	def minkowski_dist(self, b, p=2):
		dx = self.x - b.x
		dy = self.y - b.y
		return pow(pow(dx, p) + pow(dy, p), 1.0/float(p))

	def manhattan_dist(self, b):
		dx = self.x - b.x
		dy = self.y - b.y
		return abs(dx) + abs(dy)

"""
Generate a bunch of points in gaussian clusters with random labels
	x - The upper limit of the x-axis
	y - the upper limit of the y-axis
	n - the number of points
	k - the number of clusters
"""
def gen_gauss_pts(x=500, y=500, n=100, k=9):
	points = []
	f = lambda mu, sigma : random.gauss(mu, sigma)
	for i in range(k):
		centerx = random.uniform(50, x - 50)
		centery = random.uniform(50, y - 50)
		sig = random.uniform(0, 50)
		for j in range(n//k):
			points.append(Point(f(centerx, sig), f(centery, sig), random.randint(0, k)))
	return points 

def kmeans(x=500, y=500, n=100, numk=9, iterations=20):
	points = gen_gauss_pts(x, y, n, numk)
	clusters = []
	newclusters = []
	for i in range(numk):
			clusters.append(Point(random.randint(0, x), random.randint(0, y), i))
			newclusters.append(Point(0, 0, i))

	#Update the points labels
	for i in range(iterations):
		for p in points:
			shortest_dist = x * y * numk 
			closest_k = p.label 
			for k in clusters:
				dist = k.minkowski_dist(p)
				#dist = k.manhattan_dist(p)
				#dist = k.minkowski_dist(p, 6)
				if dist < shortest_dist:
					shortest_dist = dist 
					closest_k = k.label
			p.label = closest_k
		#Update the clusters center
		num_points = [0 for _ in range(numk)]
		for p in points:
			newclusters[p.label].x = newclusters[p.label].x + p.x 
			newclusters[p.label].y = newclusters[p.label].y + p.y 
			num_points[p.label] = num_points[p.label] + 1
		for k in clusters:
			count = max(1, num_points[k.label])
			k.x = newclusters[k.label].x / count
			k.y = newclusters[k.label].y / count
		#reset newclusters
		for k in newclusters:
			k.x = 0
			k.y = 0

	return (points, clusters)

"""
Takes a list of points and turns it into 3 arrays of the x and y coordinates
and the labels. This is helpful for plotting and training
"""
def point_group_to_array(pts):
	x = list()
	y = list()
	l = list() 
	for p in pts:
		x.append(p.x)
		y.append(p.y)
		l.append(p.label)
	return (x, y, l)

"""
Plot the output of the kmeans function. The cluster centers are black *'s 
and the points themselves are color coordinated to which center they belong to
"""
def plot_kmeans(pts, clusters):
	for cluster in clusters:
		group = filter(lambda p : p.label == cluster.label, pts)
		grp_x, grp_y, _ = point_group_to_array(group)
		plt.scatter(grp_x, grp_y, s=1)
	k_x, k_y, _ = point_group_to_array(clusters)
	plt.scatter(k_x, k_y, color='black', marker='*')
	plt.show()

###############################################
# End Kmeans Helper Functions and Classes
###############################################

###############################################
# Start neural net functions and classes
###############################################

"""
GenTrainingData
	Used to generate training data for the KmeansNet. This can be optimized
	by using numpy.
	this ended up not being used because we just learn one kmeans output
"""
def genTrainingData(x, y, num_points, numk, iterations, num_training_samples):
	xs = list() #the inputs to the NN (the x, y data)
	ys = list() #expected outputs of the NN (the cluster centers)
	for i in range(num_training_samples):
		pts, cs = kmeans(x, y, num_points, numk, iterations) #don't need the clutster centers really
		px, py, pl = point_group_to_array(pts)
		# The input layer will be the x coords followed by the y coords
		# this layer also needs to be normalized (x is the max dim)
		# there is a potential bug here because I am assuming x = y for normalization
		xs.append(list(map(lambda a : a / x, px + py)))
		# the output layer is just the cluster centers.
		# These can be normalized just like the x, y data
		ys.append(list(map(lambda a : a / x, cs)))
	return (xs, ys)

class KmeansNet(torch.nn.Module):
	"""
	KmeansNet 
		Use it to learn the output of one kmeans output
	"""
	def __init__(self, in_dim, hid_dim, out_dim):
		super(KmeansNet, self).__init__()
		self.inputlayer = nn.Linear(in_dim, hid_dim)
		self.hiddenlayer = nn.Linear(hid_dim, hid_dim)
		self.outlayer = nn.Linear(hid_dim, out_dim)

	def forward(self, x):
		x = F.relu(self.inputlayer(x))
		x = F.relu(self.hiddenlayer(x))
		x = self.outlayer(x)
		return F.log_softmax(x, dim=1)
		#return x


###############################################
# End neural net functions and classes
###############################################

#########
# MAIN
#########

def main():
	# Constants
	seed = 6
	random.seed(seed) #Seed the random number generator for reproducability
	width = 500 #the coordinate space for x, (0, 500)
	height = 500 #the coordinate space for y, (0, 500)
	num_points = 1000
	numk = 10 
	num_iters = 25
	num_samples = 20

	"""
	#sample usage of the kmeans
	pts, cs = kmeans(width, height, num_points, numk, num_iters)
	plot_kmeans(pts, cs)
	"""
	##########################
	# Prepare the plot stuff
	##########################
	fig = plt.figure()
	camera = Camera(fig)
	h = 0.02
	x_min, x_max = 0, 1
	y_min, y_max = 0, 1
	xx, yy = np.meshgrid(np.arange(x_min, x_max, h, dtype='float32'), np.arange(y_min, y_max, h, dtype='float32'))
	test_data = torch.tensor(np.c_[xx.ravel(), yy.ravel()])

	# Train The Neural net
	#datax, datay = genTrainingData(width, height, num_points, numk, num_iters, num_samples)
	pts, cs = kmeans(width, height, num_points, numk, num_iters)
	px1, px2, py = point_group_to_array(pts)
	normalize = lambda x, m : map(lambda a : a / m, x)
	datax = list(zip(normalize(px1, width), normalize(px2, height)))
	datay = py 
	datax = torch.tensor(datax)
	datay = torch.tensor(datay)
	net = KmeansNet(2, 100, numk)
	learning_rate = 0.2
	m = 0.9
	optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate, momentum=m)
	#loss_fxn = torch.nn.MSELoss()
	loss_fxn = torch.nn.CrossEntropyLoss()
	n_epochs = 250

	ins = Variable(datax)
	outs = Variable(datay)
	for epoch in range(n_epochs):
		prediction = net(ins)
		loss = loss_fxn(prediction, outs)
		optimizer.zero_grad()
		loss.backward() 
		optimizer.step()

		if epoch % 2 == 0:
			test_out = net(test_data)
			Z = test_out.data.numpy()
			Z = np.argmax(Z, axis=1)
			Z = Z.reshape(xx.shape)
			plt.contourf(xx, yy, Z, cmap=plt.cm.Set1, alpha=0.5)
			plt.scatter(datax.data.numpy()[:, 0], datax.data.numpy()[:,1], c=py, s=40, cmap=plt.cm.Set1)
			plt.text(0.5, 0.05, f"Loss={loss.data.numpy()}", fontdict={'size':12, 'color': 'red'})
			plt.xlim(xx.min(), xx.max())
			plt.ylim(yy.min(), yy.max())
			#plt.show()
			camera.snap()

	animation = camera.animate()
	animation.save('kmeans.mp4')	


if __name__ == '__main__':
	main()